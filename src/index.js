import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import MapView from './MapView';

ReactDOM.render(
  <React.StrictMode>
    <MapView/>
  </React.StrictMode>,
  document.getElementById('root')
);


reportWebVitals();
