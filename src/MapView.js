import React from 'react';
import { useState } from 'react';
import InfosView from './InfosView';
import L from "leaflet";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import fabrique from './ville.json';

let occitanie = [43.50,2.50]

var redIcon = new L.Icon({
  iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});



function MapView(){
  const [ind, setInd] = useState(0)
  const [GeoLocationView, setGeoLocation] = useState([0,0]);
  const [weather,setWeather] = useState({});
  if ("geolocation" in navigator){
    navigator.geolocation.getCurrentPosition(function(position){
      setGeoLocation([position.coords.latitude, position.coords.longitude]);
    })
  }
  function WeatherViews(latitude,longitude) {
  
    fetch("http://api.openweathermap.org/data/2.5/weather?lat=" + latitude + "&lon="+ longitude + "&appid=53a4a7325ae57360aacc7ab28b92bc48&units=metric&lang=fr"
    )
    .then(response => {
      let o = response.json()
      return o
    })
    .then((obj) => {
      setWeather(obj)
    })
    .catch(e => console.error(e))
  }
    return (
        <div id="inf">
          <MapContainer center={occitanie} zoom={7}>
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png" />

          {fabrique.map(
            (location, i) => {
              return(
                <Marker  icon={redIcon}
                  position={[location.location.latitude, location.location.longitude]} 
                  key={location.nom}
                  eventHandlers={{
                    click: () => {
                      setInd(i)
                      WeatherViews(location.location.latitude, location.location.longitude)
                    }
                  }}
                  >
                    <Popup>
                      <h4>{location.nom}</h4>
                      <h5>Température:</h5>{(weather.weather)? weather.main.temp:""}°C
                      <br/>
                      <h5>Vent:</h5>{(weather.weather)? weather.wind.speed:""}Km/h
                      <br/>
                      {(weather.weather)? weather.weather[0].description:""}
                      <img src={(weather.weather)? `https://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`:""} alt="weather icon" />
                    </Popup>
                  </Marker>
              )
            }
          )}
        <Marker position={GeoLocationView}>
          <Popup>
            <h5>Vous êtes ici</h5>
            <br/>
            <h5>Température:</h5>{(weather.weather)? weather.main.temp:""}°C
            <br/>
            <h5>Vent:</h5>{(weather.weather)? weather.wind.speed:""}Km/h
            <br/>
            {(weather.weather)? weather.weather[0].description:""}
            <img src={(weather.weather)? `https://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`:""} alt="weather icon" />
          </Popup>
        </Marker>
        </MapContainer>
        <InfosView i={ind}></InfosView>
      </div>
      )
}

export default MapView;