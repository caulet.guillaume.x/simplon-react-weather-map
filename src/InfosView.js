import fabrique from './ville.json'


function InfosView(props){
    return(
        <div id="infos" role="button" className="smp-card factory-card keyboard-focusable">
            <h3 className="card-title">
                {fabrique[props.i].nom}
                <span className="card-title-info"></span>
            </h3>
            <div className="card-body">
                <i className="material-icons" >location_on</i>
                <div className="card-address">
                    {fabrique[props.i].adresse}
                </div>
            </div>                         
        </div>
  )
}
export default InfosView;